const aslide = {
  getCategory: {
    url: '/api/category',
    method: 'get',
  },
};

export default aslide;
