const article = {
  getRecommendList: {
    method: 'get',
    url: '/api/article/recommend',
  },
};

export default article;
