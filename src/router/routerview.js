import { Switch, Route, Redirect } from 'react-router-dom';
import _ from 'lodash';
// import routes from './routerConfig';
function RouterView({ routes }) {
  let routerArr = routes.filter((item) => !item.to && item.path !== '*'); //筛选出来不包含重定向和通配符
  let redirectArr = routes
    .filter((item) => item.to)
    .map((item1, index1) => (
      <Redirect exact key={index1} from={item1.path} to={item1.to} />
    ));
  //包含通配符的
  let notArr = routes.find((item) => item.path == '*');
  return (
    <Switch>
      {routerArr &&
        routerArr
          .map((item, index) => (
            <Route
              key={index}
              path={item.path}
              render={(props) => {
                document.title = item.meta ? item.meta.title : '八维创作平台';
                //先判断是否需要守卫
                let isAuth = item.meta && item.meta.isAuth; //IsLogin
                if (isAuth && _.isFunction(isAuth)) {
                  let Com = isAuth(item.component);
                  return <Com {...props} child={item.children} />;
                }
                return <item.component {...props} child={item.children} />;
              }}
            />
          ))
          .concat(redirectArr)}
      {notArr && (
        <Route
          path={notArr.path}
          render={(props) => {
            return <notArr.component {...props} />;
          }}
        />
      )}
    </Switch>
  );
}

export default RouterView;
