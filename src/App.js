import './App.css';
import React, { Component } from 'react';
import './styles/common.less';
import RootRouter from './router/index.js';

import { IntlProvider, FormattedMessage, FormattedNumber } from 'react-intl';
import { inject, observer } from 'mobx-react';

@inject('titleStore')
@observer
class App extends Component {
  render() {
    // console.log(this.props.age);
    let { message, locale } = this.props.titleStore;
    return (
      <div className="container">
        <IntlProvider messages={message} locale={locale}>
          <RootRouter />
        </IntlProvider>
      </div>
    );
  }
}

export default App;
// export default App; withRouter(App)
