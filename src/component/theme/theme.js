import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

@inject('titleStore')
@observer
class Theme extends Component {
  handleClickColor = (flag) => {
    // this.setState(
    //   {
    //     flag: !this.state.flag,
    //   },
    //   () => {
    //     window.less.modifyVars(this.state.flag ? light : dark);
    //   }
    // );

    // flag:true  白色  false 黑色
    this.props.titleStore.changeTheme(flag);
  };
  render() {
    const {
      titleStore: { flag },
    } = this.props;

    return (
      <button onClick={() => this.handleClickColor(!flag)}>
        {flag ? '白色' : '黑色'}
      </button>
    );
  }
}

export default Theme;
