import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

// Article.addNUm = ()={} static

@inject('titleStore', 'numStore')
@observer
class Article extends Component {
  state = {
    flag: true,
  };
  handleClick = () => {
    this.setState({
      flag: false,
    });
  };
  handleTitle = () => {
    this.props.titleStore.setTitle('6666');
  };
  handleNum = (num) => {
    this.props.numStore.addNum(num);
  };
  render() {
    const { flag } = this.state;
    let { num } = this.props.numStore;
    if (!flag) {
      throw new Error('错误^^^^^^^');
    }
    return (
      <div>
        <h3>{this.props.titleStore.title}</h3>
        <p>{num}</p>
        <button onClick={() => this.handleNum(num + 1)}>加加</button>
        <button onClick={this.handleTitle}>切换标题</button>
        <button onClick={this.handleClick}>点击</button>
      </div>
    );
  }
}

// function inject(arg){ //''
//   return (Com)=>{
//      return class extends Component{
//        render(){
//          return <Consumer>
//            {
//              val => { //{}
//                let res = val[arg];
//                return <Com  {...res}/>
//              }
//            }
//          </Consumer>
//        }
//      }
//   }
// }

export default Article;
// export default inject('titleStore')(observer(Article));
