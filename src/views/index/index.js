import React, { Component } from 'react';
import RouterView from '../../router/routerview';
import index from './index.module.less';
import classnames from 'classnames';
import { IndexRoutes } from '@/router/routerConfig';
import { Menu } from 'antd';
import { NavLink } from 'react-router-dom';
import Theme from '@/component/theme/theme';
import { inject, observer } from 'mobx-react';
import { FormattedMessage } from 'react-intl';
import api from '@/api/index';
console.log(api, '&&&&');
@inject('titleStore')
@observer
class Index extends Component {
  render() {
    const {
      child,
      titleStore: { flag, locale },
    } = this.props;
    return (
      <div>
        <header className={index.header}>
          <Menu
            className={index.menus}
            theme={flag ? 'light' : 'dark'}
            mode="horizontal"
            defaultSelectedKeys={['2']}
          >
            {IndexRoutes.map((item, index) => {
              return (
                <Menu.Item key={index}>
                  <NavLink to={item.path}>
                    <FormattedMessage id={item.meta.title} />
                  </NavLink>
                </Menu.Item>
              );
            })}
          </Menu>
          <button onClick={() => this.props.titleStore.changeLange()}>
            {locale === 'zh-CN' ? '中文' : '英文'}
          </button>
          <Theme />
        </header>
        <section className={classnames('container', index.container)}>
          <RouterView routes={child} />
        </section>
      </div>
    );
  }
}

export default Index;
