import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
export default class Knowledge extends Component {
  render() {
    return (
      <div>
        <FormattedMessage id="articleList" />
      </div>
    );
  }
}
