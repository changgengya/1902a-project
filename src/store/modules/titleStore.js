import { makeAutoObservable } from 'mobx';
import { dark, light } from '@/config/theme.config';
import zhCN from '@/config/langConfig/zh-CN';
import enUS from '@/config/langConfig/en-US';
const langeMessage = {
  'zh-CN': zhCN,
  'en-US': enUS,
};
class TitleStore {
  title = '八维创作平台';
  flag = true; //控制主题的变量
  locale = navigator.language; //'zh-CN'
  message = langeMessage[this.locale]; //语言配置对象
  constructor() {
    makeAutoObservable(this);
  }
  setTitle(val) {
    this.title = val;
    document.title = this.title;
  }
  changeTheme(flag) {
    //更改主题
    this.flag = flag;
    window.less.modifyVars(this.flag ? light : dark);
  }
  changeLange() {
    this.locale = this.locale === 'zh-CN' ? 'en-US' : 'zh-CN'; //语言环境
    this.message = langeMessage[this.locale]; //更改了语言配置
  }
}

export default new TitleStore();
