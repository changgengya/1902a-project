import { makeAutoObservable } from 'mobx';
class numStore {
  num = 1;
  constructor() {
    makeAutoObservable(this);
  }
  addNum(num) {
    this.num = num;
  }
}

export default new numStore();
