1. 关联仓库

- git clone 地址
- git init (会生产一个.git 文件夹) git remote add origin 地址

2. 提交

- git add . (提交到暂存区)
- git commit -m '描述' （提交到本地版本库）
- git pull origin 分支名（dev main） (从远程仓库拉下来最新的代码)
- git push origin 分支名

3. 分支

- git checkout -b 分支名 （创建并切换分支）
- git branch (查看本地分支)
- git branch -a(查看本地和远程分支)
- git meger 分支名（要合并的分支，先切到要合并的分支）

4. 状态

- git status (工作区发生变化红色的，暂存区绿色的)

5. 回滚

- git checkout . (回滚工作区)
- git reset . (先撤回到工作区) git checkout . (回滚工作区) (回滚暂存区)
- 回滚版本库 git log(查看历史提交) git reset --hard (commit_id)

6. 冲突（只能手动解决，同时修改一个文件里面的同一行代码的时候）

7. 安装插件

- git lens
- git history

8. eslint js 语法校验。 比如变量定义没有使用。箭头没有 return.

- 新建.eslintrc.js 文件
- 在里面创建规则 extends:继承语法包 rules:设定规则
- 新建忽略文件

9. babel 处理 js 文件，把所有的 js 文件编译成 es5 的语法；

- 新建.babelrc.js 文件
- 添加配置
- 移除 console.log 的插件（babel-plugin-transform-remove-console）
- 支持装饰器语法的插件（babel-plugin-transform-decorators-legacy）

10. prettier 代码风格配置

- 先下载 prettier
- 新建.pretteriterrc 文件，把拷贝的 json 放进去
- eslint 和 prettier 一起使用的时候，有冲突
- npm install eslint-config-prettier eslint-plugin-prettier --save-dev
- 在.eslintrc 文件里面添加插件和规则
- （提交之前）解决：先全局安装一个 prettier
- 执行 prettier --write .

11. husky

- . git hooks (git 的钩子)
- . lint-staged (规范暂存区的规则)
- . commitlint (规范提交信息)

12. less (antd 使用的 less)，需要先在脚手架里面配置 less

- 先在 less less-loader(7.x)
- 在 webpack.config.js 里面修改配置
- 在 less 文件里面使用 less 语法

13. 主题切换

- 下载 antd-theme-generator
- 在根目录里面配置一个 color.js 文件，然后配置项放进去
- 在 pulic 下的 html 里面，引入 less、配置全局 less,引入 less.js

14. less 配置全局变量

- npm install --save-dev style-resources-loader
- 在 webpack.config.js 里面找到 less-loader 的位置，添加一下代码

```
{
                loader: 'style-resources-loader',
                options: {
                  patterns: path.resolve(__dirname, '../src/styles/variables.less'),
                },
},
```

- 需要注意的是，配置全局的路径一定要正确

15. setState 在 setTimeout 里面使用和在原生事件里面使用的时候，都是同步的。其他的情况下是异步的。通过上下文对象来判断是同步还是异步
